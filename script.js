/*
* Функции нужны для выполнения повторяющихся действий и, как следствие - сокращения кода. Это блок кода,
* который выполняется столько раз, сколько он вызван.
*
* Функция принимает аргументы, а затем производит с ними действия. Если функции для выполнения действий нужны внешние данные,
* мы передаем их в качестве аргументов/аргумента.
*

* */

let num1 = +prompt('Enter first number', '');
while (!num1 || Number.isNaN(num1)) {
    num1 = +prompt('Enter first number, please', '');
}
let num2 = +prompt('Enter second number', '');
while (!num2 || Number.isNaN(num2)) {
    num2 = +prompt('Enter second number, please', '');
}
let operator = prompt('What operation do you want to do with your numbers?');
while (operator !== '+' && operator !== '-' && operator !== '*' && operator !== '/') {
    operator = prompt(`Error! Try again, please.
        What operation do you want to do with your numbers?`);
}

doMath(num1, num2, operator);

function doMath (firstNumber, secondNumber, math) {
    let result;
    switch (math) {
        case '+':
            result = firstNumber + secondNumber;
            break;
        case '-':
            result = firstNumber - secondNumber;
            break;
        case '*':
            result = firstNumber * secondNumber;
            break;
        case '/':
            result = firstNumber / secondNumber;
            break;
    }
console.log(result);
}
